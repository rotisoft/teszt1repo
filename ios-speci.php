<?php /*
Plugin Name: iOS Speci by 7Digits
Plugin URI: https://7digits.net
Description: iOS rendszerekhez CSS fix hozzáadása.
Version: 1.2
Author: 7Digits
Author URI: https://7digits.net
Text Domain: ios-speci
*/

$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");

//iOS ellenorzes es csak akkor fut le, ha iOS-rol nezik
if( $iPod || $iPhone || $iPad ){
    add_action('wp_head', 'sevendigits_ios_css_fix');
}

// Ez a funkcio fut le
function sevendigits_ios_css_fix(){
?>
    <style type="text/css">
        body #grve-theme-wrapper a.grve-btn.szineskeret { border: 2px solid #d25068 !important; }
    </style>
<?php
}; // end sevendigits_ios_css_fix

?>